package bar.foo.dependency.first;

///@name Imports
//@{

//@}

public final class DependencyFirst
{
    ///@name Methods
    //@{

        public void sayDependencyFirst ( )
        {
            System.out.println( "I am dependency-first" );
        }

    //@}
}
