package bar.foo.dependency.second;

import bar.foo.dependency.first.DependencyFirst;

///@name Imports
//@{

//@}

public final class DependencySecond
{
    ///@name Methods
    //@{

        public void sayDependencySecond ( )
        {
            new DependencyFirst( ).sayDependencyFirst( );

            System.out.println( "I am dependency-second" );
        }

    //@}
}
